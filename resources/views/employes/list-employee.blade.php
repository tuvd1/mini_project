@extends('layouts.body')
@section('index')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Danh sách nhân viên</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">Trang chủ</li>
                        <li class="breadcrumb-item active">Danh sách nhân viên</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div colspan="3">
                                <a class="btn btn-warning float-end" href="{{ route('users.export') }}">Export User Data</a>
                            </div>
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Tên </th>
                                        <th>Email</th>
                                        <th>Chức vụ</th>
                                        <th>Phòng ban</th>
                                        @if (auth()->user()->level == 0)
                                        <th>Sửa</th>
                                        <th>Xóa</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach( $employes as $employee)

                                    <tr>
                                        <th scope="row"><?=$employee["id"]; ?></th>
                                        <td>{{ $employee->name }}</td>
                                        <td>{{ $employee->email }}</td>
                                        <td>{{ @$employee->role->position }}</td>
                                        <td>{{ @$employee->department->name }}</td>
                                        {{-- <td></td> --}}
                                        @if (auth()->user()->level == 0)
                                        <td>
                                            <a href="{{ route('employee.edit', $employee->id) }}"><button type='submit' class='btn bg-yellow    btn-flat'  name=''><i class='fa fa-edit'></i></button></a>
                                        </td>
                                        <td>
                                            <form action="{{ route('employee.destroy', $employee->id) }}" method="POST" onclick="return confirm('Are you sure?')">
                                                @csrf
                                                @method('DELETE')
                                                <button type='submit' class='btn bg-red btn-flat' data-toggle='modal' data-target='#exampleModal'><i class='fa fa-trash'></i></button></a>
                                            </form>
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach

                                </tbody>

                            </table>
                            <thead style="margin-left: 950px">{{ $employes->links()  }}</thead>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection


