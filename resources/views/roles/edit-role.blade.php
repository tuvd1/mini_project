@extends('layouts.body')
@section('index')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <body>
                        <form style="margin-left: 650px ;" class="row g-3 needs-validation"
                        action="{{ route('role.update', $role->id) }}" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                            <input type="hidden" name="id" value="{{ $role->id }}">
                        <div class="col-md-2">
                            <label for="validationCustom01" class="form-label">Chức vụ</label>
                            <input type="text" class="form-control" id="validationCustom01" name="position"
                            placeholder="" value="{{ $role->position }}">
                        </div>
                        <div class="col-12">
                        <button class="btn btn-primary" type="submit" style="margin-top:10px;">Nhấn để sửa !</button>
                        </div>
                        </form>
                    </body>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
