@extends('layouts.body')
@section('index')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                <body>
                    <form style="margin-left: 650px;" class="row g-3 needs-validation"
                        action="{{ route('department.store') }}" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-2">
                            <label for="validationCustom01" class="form-label">Tên phòng ban</label>
                            <input type="text" class="form-control" id="validationCustom02"
                                name="name" placeholder="Nhập phòng ban" required>
                        </div>
                        <div class="col-md-2">
                            <label for="validationCustom01" class="form-label">Địa chỉ</label>
                            <input type="text" class="form-control" id="validationCustom02"
                                name="address" placeholder="Địa chỉ" required>
                        </div>
                        <div class="col-12" style="margin-top:10px; ">
                            <button class="btn btn-primary" type="submit" >Thêm phòng ban</button>
                        </div>
                    </form>
                </body>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
{{-- <form action="/song" method="post">
    @csrf
    Tên bài hát<br> <input type="text" name="tenBaiHat"><br>
    <button type="submit">Submit</button>
</form> --}}
