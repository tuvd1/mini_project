<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Models\Employee;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;

class EmployesExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Employee::with('role', 'department')->get();
    }
    public function map($employes): array
    {
        return [
            $employes->id,
            $employes->name,
            $employes->role->position,
            $employes->department->name,
            $employes->email,
        ];
    }
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function headings(): array
    {
        return ["ID", "Tên nhân viên", "Chức vụ", "Phòng ban", "Email"];
    }
}
