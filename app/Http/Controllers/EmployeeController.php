<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Department;
use App\Models\Role;
use Illuminate\Pagination\LengthAwarePaginator;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['isAdmin'])->except(['index']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employes = Employee::with('role', 'department')->paginate(3);
        return view('employes.list-employee', [
            'employes' => $employes,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employes = Employee::all();
        $roles = Role::all();
        $departments = Department::all();
        return view('employes.create-employee', [
            'employes'=>$employes,
            'roles'=>$roles,
            'departments'=>$departments
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employee = Employee::create([
            'name' => $request->input('ten'),
            'email' => $request->input('mail'),
            'role_id' => $request->input('chucvu'),
            'department_id' => $request->input('phongban'),
        ]);
        $employee->save();
        return redirect('employee');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employes = Employee::find($id);
        $employes->load('role', 'department');
        $roles = Role::all();
        $departments = Department::all();
        return view('employes.edit-employee', [
            'employes'=> $employes,
            'roles'=>$roles,
            'departments'=>$departments,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = Employee::where('id', $id)
            ->update([
            'name'=>$request->input('ten'),
            'email'=>$request->input('mail'),
            'role_id' => $request->input('chucvu'),
            'department_id' => $request->input('phongban'),
        ]);
        return redirect('/employee');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::find($id);
        $employee->delete();
        return redirect('/employee');
    }

}
