<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\UsersExport;
use App\Exports\EmployesExport;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\User;

class UserController extends Controller
{
    public function export()
    {
        return Excel::download(new EmployesExport, 'users.xlsx');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function isAdmin(): bool
    {
        return in_array($this->email, [
            'admin@gmail.com',
        ]);
    }
}
