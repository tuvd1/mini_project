<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('baihat', function (Blueprint $table) {
            $table->id();
            $table->string('tenBaiHat');
            $table->unsignedBigInteger('casi_id');
            $table->timestamps();
            $table->foreign('casi_id')
                  ->references('id')
                  ->on('casi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('baihat');
    }
};
